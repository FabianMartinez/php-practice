<?php

require_once 'vendor/autoload.php';

use App\Models\{Job, Project, Printable};

$job1 = new Job('PHP Dev', 'This is an awesome job!!!');
$job1->visible = true;
$job1->months = 48;

$job2 = new Job('Python Dev', 'Doing a lot of scripts for my job!!!' );
$job2->visible = true;
$job2->months = 98;

$job3 = new Job('Scala Dev', 'I love working with this technology in backend');
$job3->visible = true;
$job3->months = 12;

$job4 = new Job('Javascript Dev', 'I made a lot of games with JavaScript');
$job4->visible = true;
$job4->months = 24;

$project1 = new Project('BRIGHTYARD: Project with Laravel, Vue and TailwindCSS', 'It took 3 years to elaborate one of the most stable platforms in the internet with this technologies.');
$project1->visible = true;
$project1->months = null;

$project2 = new Project('BLACKSHIELD: stronger Anti-Spyware', 'This was an interesting project that was made with a Redux Interface and a functionality made by 5 engineer incluiding me. We used Python scripts.');
$project2->visible = true;
$project2->months = null;

$jobs = [
    $job1,
    $job2,
    $job3,
    $job4 
  ]; 

$projects = [
    $project1,
    $project2
  ];
  
  function printElement (Printable $job) {
  
    echo '<li class="work-position">';
    echo '<h5>' . $job->getTitle() . '</h5>';
    echo '<p>' . $job->getDescription() . '</p>';
    echo '<p>' . $job->getDurationAsString() . '</p>';
    echo '<p>' . $job->getStatus() . '</p>';
    echo '<strong>Achievements:</strong>';
    echo '<ul>';
    echo '<li>Lorem ipsum dolor sit amet, 80% consectetuer adipiscing elit.</li>';
    echo '<li>Lorem ipsum dolor sit amet, 80% consectetuer adipiscing elit.</li>';
    echo '<li>Lorem ipsum dolor sit amet, 80% consectetuer adipiscing elit.</li>';
    echo '</ul>';
    echo '</li>';
  
  }

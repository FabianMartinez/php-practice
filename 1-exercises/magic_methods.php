<?php

class Car {
    private $brand;
    public $model;
    public $color;
    public $price;
    public $key;


    /*  LA FUNCIÓN "__CONSTRUCT" TIENE COMO OBJETIVO INICIALIZAR LOS PARÁMETROS DADOS
        PARA QUE CUALQUIER OBJETO PUEDA SER INICIALIZADO DE FORMA INMEDIATA ANTES DE SER
        USADO.


        UN EJEMPLO DE ESTO MISMO, ES LO QUE HACEMOS CON EL CONSTRUCTOR DE ABAJO, PASANDOLE
        PARAMETROS COMO EL BRAND Y MODEL.
    */
    public function __construct($brand, $model)
    {
        $this->setBrand($brand);
        $this->setModel($model);
    }

    public function setBrand ($carBrand) {
        if ($carBrand == '') {
            $this->brand = 'UNDEFINED BRAND';
        }else{
            $this->brand = $carBrand;
        }
    }

    public function setModel ($carModel) {
        if ($carModel == '') {
            $this->model = 'UNDEFINED MODEL';
        }else{
            $this->model = $carModel;
        }
    }

    public function getBrand () {
        return $this->brand;
    }

    public function getModel () {
        return $this->model;
    }

    public function getKey () {
        return $this->key;
    }

    
    /*  EL METODO "__DESTRUCT" SERÁ LLAMADO TAN PRONTO COMO NO HAYAN OTRAS REFERENCIAS DE UN OBJETO
        DETERMINADO, O EN CUALQUIER OTRA CIRCUNSTANCIA DE FINALIZACIÓN.
    */
    public function __destruct()
    {
        echo "<br/>";
        echo "This will be going to explode";
    }
}

$car1 = new Car('Ferrari', 'LaFerrari');
$car1->color = 'Red';
$car1->price = 450000;
$car1->key = 1001;

$car2 = new Car('Bugatti', 'Chiron');
$car2->color = 'Red';
$car2->price = 2500000;
$car2->key = 1002;

$car3 = new Car('Dodge', 'Durango SRT Hellcat');
$car3->color = 'Metallic Gray';
$car3->price = 80000;
$car3->key = 1003;

$cars = [
    $car1,
    $car2,
    $car3
];


function printCar ($vehicle) {

    echo '<h2>' . $vehicle->getBrand() . '</h2>';
    echo '<h4>' . $vehicle->getModel() . '</h4>';
    echo '<p>' . $vehicle->color . '</p>';
    echo '<p>' . "USD $" . $vehicle->price . '</p>';
    echo '<p>' . $vehicle->getKey() . '</p>';

}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php

        // VARIABLES AND PRINTING

        /* $myName = "Fabián";
        echo "Hola mi nombre es $myName <br/>";

        echo "6 + 4 es " , (6 + 4) , "<br/>";
        echo "6 - 4 es " , (6 - 4) , "<br/>"; 
        echo "6 * 4 es " , (6 * 4) , "<br/>"; 
        echo "6 / 4 es " , (6 / 4) , "<br/>";
        echo "6 % 4 es " , (6 % 4) , "<br/>";
        


        // CONDITIONALS

        $miEdad = 14;

        if($miEdad >= 18) {
            echo "Tienes más de 18 años";
        }else{
            echo "Eres menor de edad <br/>";
        } 

        for ($i = 0; $i < 30; $i++) {
            echo "Hola mundo!!!!!!!! <br/>";
        }

        echo "<br/>";



        // SWITCH

        $username = "Fabian";
        switch($username){
            case "José":
                echo "Hola José!";
            break;

            case "Luis":
                echo "Hola Luis!";
            break;

            case "Fabia":
                echo "Hola Fabian";
            break;

            default:
                echo "No puedo adivinar tu nombre";
            break;
        } */


        // WHILE

        /* $numero = 0;

        while($numero < 10){
            echo "Número menor <br/>";
            $numero++;
        } */


        // ARRAYS

        /* $personas = array("Fabian", "Jose", "Luis", "Pedro");

        for ($i = 0; $i < 4; $i++){
            echo $personas[$i] . "<br/>";
        }

        foreach($personas as $persona){
            echo $persona . "<br/>";
        } */

        // OBJETOS

        /* $personas = array(
            "Fabian" => 20, 
            "Luis" => 30, 
            "Carmen" => 25
        );

        foreach($personas as $key => $value) {
            // echo $key . " tiene " . $value . "<br/>";
            echo "$key tiene $value <br/>";
        } */
        
        /* $cadenaDeTexto = " Hola ";
        echo strlen($cadenaDeTexto) . "<br/>";
        echo strlen(ltrim($cadenaDeTexto)) . "<br/>";
        echo strlen(rtrim($cadenaDeTexto)) . "<br/>"; */

        
        
        /* $usuario = "Fabian";
        
        printf("Mi nombre es %s", $usuario); */


        /* $mensaje = "Este es un mensaje de prueba";

        echo strtoupper($mensaje) . "<br/>";
        echo strtolower($mensaje) . "<br/>";

        print_r(explode(' ', $mensaje)); */


        // FUNCTIONS 
        /* $A = 20;
        $B = 40;

        function suma ($numA, $numB) {
            return $numA + $numB;
        }

        echo "A = $A / B = $B" . "<br/>";
        echo "La suma de A ($A) y B ($B) es " . suma($A, $B); */


        //CONSULTA DE NAVEGADOR
        /* echo $_SERVER['HTTP_USER_AGENT']; */

        //VARIABLE RESOURCE
        /* $res = fopen("c:\\dir\\file.txt", "r"); */

        $personas = [
            'Fabian Martinez',
            'Jose Gomez',
            'Alisson Becker'
        ];

        $obj_personas = [
            ['Nombre' => "Fabian Martinez",
            'Edad' => 23,
            'Programador' => true],
            ['Nombre' => "George Watson",
            'Edad' => 43,
            'Programador' => false],
            ['Nombre' => "John Lennox",
            'Edad' => 33,
            'Programador' => true]
        ];

        echo $obj_personas[0]['Nombre']['Edad'] . "<br/>";
        echo $obj_personas[1]['Edad'] . "<br/>";
        echo $obj_personas[2]['Edad'] . "<br/>";
    ?>
</body>
</html>

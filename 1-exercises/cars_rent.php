<?php 

require('magic_methods.php')

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cars</title>
    <style>
        .body{
            background-color: lightblue;
            font-family: 'Courier New', Courier, monospace;
        }
    </style>
</head>
<body>
    <?php 
    
    for ($i = 0; $i < 3; $i++) {
        printCar($cars[$i]);
    }
        
    ?>
</body>
</html>

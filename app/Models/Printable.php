<?php

namespace App\Models;

interface Printable {
    public function getDescription();
    public function getTitle();
    public function getDurationAsString();
    public function getStatus();
}
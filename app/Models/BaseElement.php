<?php

namespace App\Models;


class BaseElement implements Printable {
    private $title;
    public $description;
    public $visible;
    public $months;
  
    public function __construct($title, $description) {
      $this->setTitle($title);
      $this->description = $description;
    }
  
    public function setTitle ($t) {
      if ($t == '') {
        $this->title = 'N/A';
      }else{
        $this->title = $t;
      }
    }
  
    public function getTitle () {
      return $this->title;
    }
  
    public function getDurationAsString () {
      $years = floor($this->months / 12);
      $extraMonths = $this->months % 12;
    
      if ($extraMonths == 12 or $extraMonths == 0) {
        return "$years years";
      }else{
        return "$years years $extraMonths months";
      }
    }

    public function getDescription () {
      return $this->description;
    }

    public function getStatus () {
      echo "Realizando operaciones!";
    }
  
  }
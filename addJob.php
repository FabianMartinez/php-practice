<?php 

var_dump($_GET);
var_dump($_POST);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Adding job</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B"
    crossorigin="anonymous">
  <link rel="stylesheet" href="style.css">
</head>
<body>
    <h2>Add your job:</h2>
    <form action="addJob.php" method="post">
        <label for="">Title:</label><br>
        <input type="text" placeholder="Put your last job" name="title"><br>
        <label for="">Description:</label><br>
        <input type="text" placeholder="What it was about?" name="description"><br>
        <button type="submit">Submit</button>
    </form>
</body>
</html>